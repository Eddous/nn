using System;

namespace NN
{

    public class Rand
    {
        static Random r = new Random();
        public static double Next(double bound) // cca <bound,bound)
        {
            return bound * (2 * r.NextDouble() - 1);
        }
        public static int NextInt(int bound)
        { // <0,bound)
            return r.Next(bound);
        }

        public static bool NextBool(double num)
        { //percent to be true
            return r.NextDouble() < num;
        }
    }
}