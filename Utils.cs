using System;
using System.Collections.Generic;
namespace NN
{
    class Utils
    {
        public static List<List<TD>> GetRandomBatches(List<TD> points, int batchSize) //it wont delete from points!
        {
            var copy = new List<TD>(points);
            var batches = new List<List<TD>>();
            while (copy.Count != 0)
            {
                batches.Add(RandomSub(copy, batchSize, true));
            }
            return batches;
        }

        public static List<E> RandomSub<E>(List<E> list, int n, bool delete)
        {
            if (!delete)
            {
                return RandomSub(new List<E>(list), n, true);
            }
            List<E> newList = new List<E>();
            while (newList.Count != n && list.Count != 0)
            {
                int tmp = Rand.NextInt(list.Count);
                newList.Add(list[tmp]);
                list.RemoveAt(tmp);
            }
            return newList;
        }
    }
}