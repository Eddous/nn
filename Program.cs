﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NN
{
    class Program
    {
        static void Main(string[] args)
        {

            NN nn = new NN(new int[] { 784, 100, 10 });

            List<TD> train = GetPoints("./mnist/train-labels.idx1-ubyte", "./mnist/train-images.idx3-ubyte");
            List<TD> test = GetPoints("./mnist/t10k-labels.idx1-ubyte", "./mnist/t10k-images.idx3-ubyte");
            Supervised.Train(nn, train, test, 0.1, 50000, 1, 5, 1000);
        }


        private static List<TD> GetPoints(String labelPath, String dataPath)
        {
            var points = new List<TD>();
            byte[] labelB = File.ReadAllBytes(labelPath);
            byte[] pixelB = File.ReadAllBytes(dataPath);
            for (int l = 8; l < labelB.Count<byte>(); l++)
            {
                double[] label = new double[10];
                label[labelB[l]] = 1;
                double[] pixel = new double[784];
                for (int p = 0; p < 784; p++)
                {
                    pixel[p] = ((double)pixelB[16 + ((l - 8) * 784) + p]) / 256;
                }
                points.Add(new TD(pixel, label));
            }
            return points;
        }
        private static void print(Tuple<double[], double[]> inout)
        {
            var input = inout.Item1;
            var output = inout.Item2;
            for (int pixel = 0; pixel < input.Count<double>(); pixel++)
            {
                if (pixel % 28 == 0)
                {
                    Console.WriteLine();
                }
                double number = Math.Round(input[pixel]);
                if (number == 0) Console.Write("  ");
                if (number == 1) Console.Write("0 ");
            }
            Console.WriteLine();
            for (int label = 0; label < output.Count<double>(); label++)
            {
                Console.Write(label + ": " + output[label] + "    ");
            }
        }
    }
}
