﻿//KEEP THIS SIMPLE!!!!
using System;
using System.Collections.Generic;

namespace NN
{
    class NN
    {
        private int[] nnSize;
        private int INPUT;
        private int OUTPUT;
        private int LAYERS;
        private double[][] bias;
        private double[][][] weight;
        private double[][] output;
        private double[][] biasGradient;
        private double[][][] weightGradient;

        public NN(int[] size)
        {
            this.nnSize = size;
            this.LAYERS = size.Length;
            this.OUTPUT = size[LAYERS - 1];
            bias = new double[nnSize.Length][];
            output = new double[nnSize.Length][];
            biasGradient = new double[nnSize.Length][];
            weight = new double[nnSize.Length][][];
            weightGradient = new double[nnSize.Length][][];
            for (int layer = 1; layer < nnSize.Length; layer++)
            {
                bias[layer] = new double[nnSize[layer]];
                biasGradient[layer] = new double[nnSize[layer]];
                output[layer] = new double[nnSize[layer]];
                weight[layer] = new double[nnSize[layer]][];
                weightGradient[layer] = new double[nnSize[layer]][];
                for (int neuron = 0; neuron < nnSize[layer]; neuron++)
                {
                    weight[layer][neuron] = new double[nnSize[layer - 1]];
                    weightGradient[layer][neuron] = new double[nnSize[layer - 1]];
                    bias[layer][neuron] = Rand.Next(1);
                }
            }
        }

        public double[] Compute(double[] input)
        {
            if (input.Length != nnSize[0]) throw new Exception("wrong size");
            output[0] = input;
            for (int layer = 1; layer < nnSize.Length; layer++)
            {
                for (int neuron = 0; neuron < nnSize[layer]; neuron++)
                {
                    output[layer][neuron] = bias[layer][neuron];
                    for (int prevNeuron = 0; prevNeuron < nnSize[layer - 1]; prevNeuron++)
                    {
                        output[layer][neuron] += output[layer - 1][prevNeuron] * weight[layer][neuron][prevNeuron];
                    }
                    if (layer != nnSize.Length - 1) output[layer][neuron] = leakyReLu(output[layer][neuron]);
                    else output[layer][neuron] = Sigmoid(output[layer][neuron]);
                }
            }
            return output[nnSize.Length - 1];
        }

        public void Train(List<TD> points, double ETA)
        {
            Reset();
            foreach (TD point in points)
            {
                ComputeGradients(point);
            }
            Standartizate(points.Count);
            Update(ETA);

        }

        private void Reset()
        {
            for (int layer = 1; layer < nnSize.Length; layer++)
            {
                biasGradient[layer] = new double[nnSize[layer]];
                for (int neuron = 0; neuron < nnSize[layer]; neuron++)
                {
                    weightGradient[layer][neuron] = new double[nnSize[layer - 1]];
                }
            }
        }
        private void Standartizate(int numberOfPoints)
        {
            for (int layer = 1; layer < nnSize.Length; layer++)
            {
                for (int neuron = 0; neuron < nnSize[layer]; neuron++)
                {
                    biasGradient[layer][neuron] /= numberOfPoints;
                    for (int prevNeuron = 0; prevNeuron < nnSize[layer - 1]; prevNeuron++)
                    {
                        weightGradient[layer][neuron][prevNeuron] /= numberOfPoints;
                    }
                }
            }
        }
        private void Update(double ETA)
        {
            for (int layer = 1; layer < nnSize.Length; layer++)
            {
                for (int neuron = 0; neuron < nnSize[layer]; neuron++)
                {
                    bias[layer][neuron] -= ETA * biasGradient[layer][neuron];
                    for (int prevNeuron = 0; prevNeuron < nnSize[layer - 1]; prevNeuron++)
                    {
                        weight[layer][neuron][prevNeuron] -= ETA * weightGradient[layer][neuron][prevNeuron];
                    }
                }
            }
        }

        private void ComputeGradients(TD point) //input,output
        {
            double[] computed = Compute(point.input);
            if (computed.Length != output[nnSize.Length - 1].Length || point.target.Length != output[nnSize.Length - 1].Length) throw new Exception("wrong");
            //init tmp
            double[][] tmpBiasGradient = new double[nnSize.Length][];
            for (int layer = 1; layer < nnSize.Length; layer++)
            {
                tmpBiasGradient[layer] = new double[nnSize[layer]];
            }
            //first layer
            for (int neuron = 0; neuron < nnSize[nnSize.Length - 1]; neuron++)
            {
                tmpBiasGradient[LAYERS - 1][neuron] = point.gradient * LossDerivate(computed[neuron], point.target[neuron]) * SigmoidDerivate(output[LAYERS - 1][neuron]);
            }
            //rest
            for (int layer = nnSize.Length - 2; layer > 0; layer--)
            {
                for (int neuron = 0; neuron < nnSize[layer]; neuron++)
                {
                    tmpBiasGradient[layer][neuron] = 0;
                    for (int nextNeuron = 0; nextNeuron < nnSize[layer + 1]; nextNeuron++)
                    {
                        tmpBiasGradient[layer][neuron] += tmpBiasGradient[layer + 1][nextNeuron] * weight[layer + 1][nextNeuron][neuron] * leakyReLuDerivate(output[layer][neuron]);
                    }
                }
            }
            //add local gradient to global
            for (int layer = 1; layer < nnSize.Length; layer++)
            {
                for (int neuron = 0; neuron < nnSize[layer]; neuron++)
                {
                    biasGradient[layer][neuron] += tmpBiasGradient[layer][neuron];
                    for (int prevNeuron = 0; prevNeuron < nnSize[layer - 1]; prevNeuron++)
                    {
                        weightGradient[layer][neuron][prevNeuron] += output[layer - 1][prevNeuron] * tmpBiasGradient[layer][neuron];
                    }
                }
            }
        }

        private static double leakyReLu(double x)
        {
            if (0 <= x) return x;
            return 0.01 * x;
        }

        private static double leakyReLuDerivate(double x)
        {
            if (0 <= x) return 1;
            return 0.01;
        }

        private static double Sigmoid(double x)
        {
            return 1 / (1 + Math.Pow(Math.E, -x));
        }

        private static double SigmoidDerivate(double computed) //note use already computed value of sigmoid
        {
            return computed * (1 - computed);
        }

        private static double LossDerivate(double computed, double desired)
        {
            return 2 * (computed - desired);
        }

        //Its used in NN_Utilities
        public static double Loss(double[] computed, double[] desired)
        {
            double sum = 0;
            for (int i = 0; i < computed.Length; i++)
            {
                sum += Math.Pow((computed[i] - desired[i]), 2);
            }
            return sum / computed.Length;
        }
    }
}
//-----------
//---DEBUG---
//-----------

/*
public void PrintOutput()
{
    Console.WriteLine("Output");
    for (int layer = 0; layer < nnSize.Length; layer++)
    {
        Console.WriteLine("   layer number " + layer);
        for (int neuron = 0; neuron < nnSize[layer]; neuron++)
        {
            Console.WriteLine("      " + neuron + ": " + output[layer][neuron]);
        }
    }
}

public void PrintBiasGradient()
{
    Console.WriteLine("BiasGradient");
    for (int layer = 1; layer < nnSize.Length; layer++)
    {
        double sum = 0;
        Console.WriteLine("   layer number " + layer);
        for (int neuron = 0; neuron < nnSize[layer]; neuron++)
        {
            sum += Math.Abs(biasGradient[layer][neuron]);
            //Console.WriteLine("      " + neuron + ": " + biasGradient[layer][neuron]);
        }
        Console.WriteLine("   avarage: " + sum / nnSize[layer]);
    }
}

public void PrintWeightGradient()
{
    Console.WriteLine("WeightGradient");
    for (int layer = 1; layer < nnSize.Length; layer++)
    {
        Console.WriteLine("   layer number " + layer);
        for (int neuron = 0; neuron < nnSize[layer]; neuron++)
        {
            Console.WriteLine("      neuron" + neuron);
            for (int prevNeuron = 0; prevNeuron < nnSize[layer - 1]; prevNeuron++)
            {
                Console.WriteLine("         prevNeuron" + prevNeuron + ": " + weightGradient[layer][neuron][prevNeuron]);
            }
        }
    }
}

public void PrintBias()
{
    Console.WriteLine("Bias");
    for (int layer = 1; layer < nnSize.Length; layer++)
    {
        Console.WriteLine("   layer number " + layer);
        for (int neuron = 0; neuron < nnSize[layer]; neuron++)
        {
            Console.WriteLine("      " + neuron + ": " + bias[layer][neuron]);
        }
    }
}

public void PrintWeight()
{
    Console.WriteLine("Weight");
    for (int layer = 1; layer < nnSize.Length; layer++)
    {
        Console.WriteLine("   layer number " + layer);
        for (int neuron = 0; neuron < nnSize[layer]; neuron++)
        {
            Console.WriteLine("      neuron" + neuron);
            for (int prevNeuron = 0; prevNeuron < nnSize[layer - 1]; prevNeuron++)
            {
                if (prevNeuron != 736)
                {
                    continue;
                }
                Console.WriteLine("         prevNeuron" + prevNeuron + ": " + weight[layer][neuron][prevNeuron]);
            }
        }
    }
}

}
}

/**/
