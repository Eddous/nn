using System;
using System.Collections.Generic;
namespace NN
{
    class Supervised
    {
        public static void Train(NN nn, List<TD> points, double testTrainRatio, double ETA, int iteration, int batchSize, int nTesting, int nTestingData) //number of mesurement
        {
            List<TD> train = points;
            List<TD> test = new List<TD>();
            while (test.Count / train.Count < testTrainRatio)
            {
                int index = Rand.NextInt(train.Count);
                test.Add(train[index]);
                train.RemoveAt(index);
            }
            Train(nn, train, test, ETA, iteration, batchSize, nTesting, nTestingData);
        }

        public static void Train(NN nn, List<TD> train, List<TD> test, double ETA, int iteration, int batchSize, int nTesting, int nTestingData)
        {
            var list = new List<String>();
            int num = iteration / (nTesting - 1);
            for (int i = 0; i < iteration; i++)
            {
                if (i % num == 0)
                {
                    list.Add(Loss(nn, test, nTestingData));
                }
                Print(list, i + "/" + (iteration - 1));
                nn.Train(Utils.RandomSub(train, batchSize, false), ETA);
            }
            list.Add(Loss(nn, test, test.Count));
            list.Add("");
            Print(list, "Done all " + iteration + "/" + iteration);


            void Print(List<String> list, String interation)
            {
                Console.Clear();
                Console.WriteLine(interation);
                Console.WriteLine();
                foreach (String s in list)
                {
                    Console.WriteLine(s);
                }

            }
        }
        private static String Loss(NN nn, List<TD> test, int n) //default, one thruth, more thruths
        {
            List<TD> copy = Utils.RandomSub(test, n, false);
            double def = 0;
            double one = 0;
            double more = 0;
            foreach (TD t in copy)
            {
                double[] computed = nn.Compute(t.input);
                def += NN.Loss(computed, t.target);
                one += oneLoss(computed, t.target);
                more += moreLoss(computed, t.target);
            }
            return (def / copy.Count + " " + one / copy.Count + " " + more / copy.Count);
        }


        private static double oneLoss(double[] computed, double[] target)
        {
            int biggest = 0;
            for (int i = 0; i < computed.Length; i++)
            {
                if (computed[biggest] < computed[i]) biggest = i;
            }
            if (target[biggest] == 1) return 0;
            return 1;
        }
        private static double moreLoss(double[] computed, double[] target)
        {
            double notCorrect = 0;
            for (int i = 0; i < computed.Length; i++)
            {
                var a = Math.Round(computed[i]);
                var b = target[i];

                if (Math.Round(computed[i]) != target[i])
                {
                    notCorrect++;
                }
            }
            return notCorrect / computed.Length;
        }
    }
}