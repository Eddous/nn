using System;
namespace NN
{

    class TD
    {
        public double[] input;
        public double[] target;
        public double gradient;
        public TD(double[] input, double[] target, double gradient = 1)
        {
            this.input = input;
            this.target = target;
            this.gradient = gradient;
        }
    }
}